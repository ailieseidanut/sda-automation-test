import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FlightFuelCalculatorCessna162Tests {

    Plane plane;

    @Before
    public void prepare() {
        plane = new Cessna162();
    }

    @Test
    public void fuelconsumption_300km_nowind_nopax() {
        // Arrange
        FlightFuelCalculator fuelCalculator = new FlightFuelCalculator(plane);

        // Act
        double consumption = fuelCalculator.calculateRequiredFuel(300, 0);

        // Assert
        Assert.assertEquals(31.914, consumption, 0.001);
    }

    @Test
    public void fuelconsumption_450km_slowwind_nopax() {
        // Arrange
        FlightFuelCalculator fuelCalculator = new FlightFuelCalculator(plane);

        // Act
        double consumption = fuelCalculator.calculateRequiredFuel(450, 0, 7);

        // Assert
        Assert.assertEquals(45.623, consumption, 0.001);
    }

    @Test
    public void fuelconsumption_450km_highOppositeWind_nopax() {
        // Arrange
        FlightFuelCalculator fuelCalculator = new FlightFuelCalculator(plane);

        // Act
        double consumption = fuelCalculator.calculateRequiredFuel(450, 0, 16);

        // Assert
        Assert.assertEquals(50.185, consumption, 0.001);
    }

    @Test
    public void fuelconsumption_450km_slowSameDirectionWind_nopax() {
        // Arrange
        FlightFuelCalculator fuelCalculator = new FlightFuelCalculator(plane);

        // Act
        double consumption = fuelCalculator.calculateRequiredFuel(450, 0, -7);

        // Assert
        Assert.assertEquals(45.623, consumption, 0.001);
    }

    @Test
    public void fuelconsumption_450km_highSameDirectionWind_nopax() {
        // Arrange
        FlightFuelCalculator fuelCalculator = new FlightFuelCalculator(plane);

        // Act
        double consumption = fuelCalculator.calculateRequiredFuel(450, 0, -16);

        // Assert
        Assert.assertEquals(41.061, consumption, 0.001);
    }

    @Test
    public void fuelconsumption_300km_nowind_1pax() {
        // Arrange
        FlightFuelCalculator fuelCalculator = new FlightFuelCalculator(plane);

        // Act
        double consumption = fuelCalculator.calculateRequiredFuel(300, 1);

        // Assert
        Assert.assertEquals(44.562, consumption, 0.001);
    }

    @Test
    public void fuelconsumption_300km_nowind_2pax() {
        // Arrange
        FlightFuelCalculator fuelCalculator = new FlightFuelCalculator(plane);

        // Act
        double consumption = fuelCalculator.calculateRequiredFuel(300, 2);

        // Assert
        Assert.assertEquals(57.21, consumption, 0.001);
    }

    @Test
    public void fuelconsumption_450km_slowwind_1pax() {
        // Arrange
        FlightFuelCalculator fuelCalculator = new FlightFuelCalculator(plane);

        // Act
        double consumption = fuelCalculator.calculateRequiredFuel(450, 1, 7);

        // Assert
        Assert.assertEquals(62.978, consumption, 0.001);
    }

    @Test
    public void fuelconsumption_450km_highOppositeWind_1pax() {
        // Arrange
        FlightFuelCalculator fuelCalculator = new FlightFuelCalculator(plane);

        // Act
        double consumption = fuelCalculator.calculateRequiredFuel(450, 1, 16);

        // Assert
        Assert.assertEquals(69.276, consumption, 0.001);
    }


}