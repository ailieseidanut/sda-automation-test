import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FlightFuelCalculatorBoeing738Tests {

  Plane plane;

  @Before
  public void prepare() {
    plane = new Boeing737_800();
  }

  @Test
  public void fuelconsumption_1000km_nowind_nopax() {
    // Arrange
    FlightFuelCalculator fuelCalculator = new FlightFuelCalculator(plane);

    // Act
    double consumption = fuelCalculator.calculateRequiredFuel(1000, 0);

    // Assert
    Assert.assertEquals(4186.8, consumption, 0.1);
  }

  @Test
  public void fuelconsumption_1000km_slowwind_nopax() {
    // Arrange
    FlightFuelCalculator fuelCalculator = new FlightFuelCalculator(plane);

    // Act
    double consumption = fuelCalculator.calculateRequiredFuel(1000, 0, 7);

    // Assert
    Assert.assertEquals(4186.8, consumption, 0.1);
  }

  @Test
  public void fuelconsumption_1000km_highOppositeWind_nopax() {
    // Arrange
    FlightFuelCalculator fuelCalculator = new FlightFuelCalculator(plane);

    // Act
    double consumption = fuelCalculator.calculateRequiredFuel(1000, 0, 16);

    // Assert
    Assert.assertEquals(4605.5, consumption, 0.1);
  }

  @Test
  public void fuelconsumption_1000km_slowSameDirectionWind_nopax() {
    // Arrange
    FlightFuelCalculator fuelCalculator = new FlightFuelCalculator(plane);

    // Act
    double consumption = fuelCalculator.calculateRequiredFuel(1000, 0, -7);

    // Assert
    Assert.assertEquals(4186.8, consumption, 0.1);
  }

  @Test
  public void fuelconsumption_1000km_highSameDirectionWind_nopax() {
    // Arrange
    FlightFuelCalculator fuelCalculator = new FlightFuelCalculator(plane);

    // Act
    double consumption = fuelCalculator.calculateRequiredFuel(1000, 0, -16);

    // Assert
    Assert.assertEquals(3768.1, consumption, 0.1);
  }

  @Test
  public void fuelconsumption_1000km_nowind_160pax() {
    // Arrange
    FlightFuelCalculator fuelCalculator = new FlightFuelCalculator(plane);

    // Act
    double consumption = fuelCalculator.calculateRequiredFuel(1000, 160);

    // Assert
    Assert.assertEquals(5835.6, consumption, 0.1);
  }

  @Test
  public void fuelconsumption_1000km_highOppositeWind_160pax() {
    // Arrange
    FlightFuelCalculator fuelCalculator = new FlightFuelCalculator(plane);

    // Act
    double consumption = fuelCalculator.calculateRequiredFuel(1000, 160, 16);

    // Assert
    Assert.assertEquals(6419.2, consumption, 0.1);
  }

  @Test
  public void fuelconsumption_1000km_highSameDirectionWind_160pax() {
    // Arrange
    FlightFuelCalculator fuelCalculator = new FlightFuelCalculator(plane);

    // Act
    double consumption = fuelCalculator.calculateRequiredFuel(1000, 160, -16);

    // Assert
    Assert.assertEquals(5252.1, consumption, 0.1);
  }

}