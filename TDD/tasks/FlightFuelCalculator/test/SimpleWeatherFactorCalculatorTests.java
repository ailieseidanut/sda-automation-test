import org.junit.Assert;
import org.junit.Test;

public class SimpleWeatherFactorCalculatorTests {

    // Arrange
    private WeatherFactorCalculator simpleWeatherCalculator = new SimpleWeatherFactorCalculator();

    // partition of equivalence values
    @Test
    public void nowind_exp_noinfluence() {
        // Arrange
        // Act
        double actualWindFactor = simpleWeatherCalculator.windFactor(0.0);
        //Assert
        Assert.assertEquals(0, actualWindFactor, 0.001);
    }

    @Test
    public void slowwind_opposite_exp_noinfluence() {
        // Arrange
        // Act
        double actualWindFactor = simpleWeatherCalculator.windFactor(7.1);
        //Assert
        Assert.assertEquals(0, actualWindFactor, 0.001);
    }

    @Test
    public void highwind_opposite_exp_positivefactor() {
        // Arrange
        // Act
        double actualWindFactor = simpleWeatherCalculator.windFactor(21.9);
        //Assert
        Assert.assertEquals(0.1, actualWindFactor, 0.001);
    }

    @Test
    public void slowwind_same_exp_noinfluence() {
        // Arrange
        // Act
        double actualWindFactor = simpleWeatherCalculator.windFactor(-1.9);
        //Assert
        Assert.assertEquals(0, actualWindFactor, 0.001);
    }

    @Test
    public void highwind_same_exp_negativefactor() {
        // Arrange
        // Act
        double actualWindFactor = simpleWeatherCalculator.windFactor(-21.9);
        //Assert
        Assert.assertEquals(-0.1, actualWindFactor, 0.001);
    }

    // border cases values
    @Test
    public void slowwind_border_opposite_exp_noinfluence() {
        // Arrange
        // Act
        double actualWindFactor = simpleWeatherCalculator.windFactor(14.99999999);
        //Assert
        Assert.assertEquals(0, actualWindFactor, 0.001);
    }

    @Test
    public void highwind_border_opposite_exp_positivefactor() {
        // Arrange
        // Act
        double actualWindFactor = simpleWeatherCalculator.windFactor(15.0);
        //Assert
        Assert.assertEquals(0.1, actualWindFactor, 0.001);
    }

    @Test
    public void slowwind_border_same_exp_noinfluence() {
        // Arrange
        // Act
        double actualWindFactor = simpleWeatherCalculator.windFactor(-14.99999999);
        //Assert
        Assert.assertEquals(0, actualWindFactor, 0.001);
    }

    @Test
    public void highwind_border_same_exp_negativefactor() {
        // Arrange
        // Act
        double actualWindFactor = simpleWeatherCalculator.windFactor(-15.0);
        //Assert
        Assert.assertEquals(-0.1, actualWindFactor, 0.001);
    }

}
