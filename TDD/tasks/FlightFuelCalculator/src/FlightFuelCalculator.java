public class FlightFuelCalculator {

    private Plane plane;
    private WeatherFactorCalculator weatherFactorCalculator;

    public FlightFuelCalculator(Plane plane) {
        this.plane = plane;
        weatherFactorCalculator = new SimpleWeatherFactorCalculator();
    }

    /**
     * Calculates expected fuel consumption for a given plane, number of passengers and planned
     * distance with reserve fuel required
     * @param plannedDistance - distance to flight
     * @param paxNo - number of passengers (with crew)
     * @return required amount of fuel in kg
     */
    public double calculateRequiredFuel(int plannedDistance, int paxNo) {
        return calculateRequiredFuel(plannedDistance, paxNo, 0);
    }

    /**
     * Calculates expected fuel consumption for a given plane, number of passengers and planned
     * distance with reserve fuel required
     * @param plannedDistance - distance to flight
     * @param paxNo - number of passengers (with crew)
     * @param windSpeed - average speed of wind during the flight
     *                    - negative value - the same direction,
     *                    - positive value - counter direction
     * @return required amount of fuel in kg
     */
    public double calculateRequiredFuel(int plannedDistance, int paxNo, double windSpeed) {
        // base (empty plane) fuel consumption
        int distanceWithReserve = plannedDistance + plane.reserveDistance();
        double fuelConsumption = distanceWithReserve * plane.fuelConsumption100km() / 100.0;

        // calculate weight influence
        // hint: use plane.zfw(), plane.mtow()

        // calculate wind influence
        // hint: use weatherFactorCalculator.windFactor()

        return fuelConsumption;
    }

}
