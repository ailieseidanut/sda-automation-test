public interface Thermometer {

    public double readTemperature();

}
