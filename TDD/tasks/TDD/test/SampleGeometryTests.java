import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SampleGeometryTests {

  SampleGeometry calc;

  @Before
  public void beforeTest() {
    calc = new SampleGeometry();
  }

  @Test
  public void testDivisible_10_2_True() {
    Assert.assertTrue(calc.isDivisibleBy(10, 2));
  }

  @Test
  public void testDivisible_10_3_False() {
    Assert.assertFalse(calc.isDivisibleBy(9, 2));
  }

  @Test
  public void testDivisible_9_3_True() {
    Assert.assertTrue(calc.isDivisibleBy(9, 3));
  }

  @Test
  public void testDivisible_9_2_False() {
    Assert.assertTrue(calc.isDivisibleBy(9, 2));
  }

  @Test
  public void testGetLowerNumber_firstLower() {
    Assert.assertEquals(3, calc.getLowerNumber(3, 50));
  }

  @Test
  public void testGetLowerNumber_secondLower() {
    Assert.assertEquals(5, calc.getLowerNumber(100, 5));
  }

  @Test
  public void testMaxNumberOfFour_16() {
    Assert.assertEquals(16, calc.getMaxNumberOfFour(3, -2, 14, 16));
  }

  @Test
  public void testMaxNumberOfFour_negative2() {
    Assert.assertEquals(-2, calc.getMaxNumberOfFour(-3, -2, -14, -16));
  }

  @Test
  public void testMaxNumberOfFour_14() {
    Assert.assertEquals(14, calc.getMaxNumberOfFour(3, 2, 14, 11));
  }

  @Test
  public void testMaxNumberOfFour_30000() {
    Assert.assertEquals(30000, calc.getMaxNumberOfFour(30000, 10000, 29999, 16));
  }

  @Test
  public void testMaxNumberAnyNumberOfValues_SingleValue_3() {
    Assert.assertEquals(3, calc.getMaxNumber(3));
  }

  @Test
  public void testMaxNumberAnyNumberOfValues_14() {
    Assert.assertEquals(14, calc.getMaxNumber(3, 2, 14, 11));
  }

  @Test
  public void testMaxNumberAnyNumberOfValues_30000() {
    Assert.assertEquals(30000, calc.getMaxNumber(30000, 10000, 29999, 16, 1000, 2, -1999999));
  }


  @Test
  public void testMaxNumberThrowException() {
    try
    {
      Assert.assertEquals(16, calc.getMaxNumber());
      Assert.fail("Should throw: 'UnsupportedOperationException' with message: 'There should be at least one value'" );
    }
    catch(UnsupportedOperationException ex) {
      Assert.assertEquals("There should be at least one value", ex.getMessage());
    }
  }

  @Test
  public void testCanCreateTriangle_2_3_4_True() {
    Assert.assertTrue("the segments: 2, 3, 4 can create triangle", calc.canCreateTriangle(2, 3, 4));
  }

  @Test
  public void testCanCreateTriangle_5_6_4_True() {
    Assert.assertTrue("the segments: 5, 6, 4 can create triangle", calc.canCreateTriangle(5, 6, 4));
  }

  @Test
  public void testCanCreateTriangle_11_10_9_True() {
    Assert.assertTrue("the segments: 11, 10, 9 can create triangle", calc.canCreateTriangle(11, 10, 9));
  }

  @Test
  public void testCanCreateTriangle_20_3_4_False() {
    Assert.assertFalse("the segments: 20, 3, 4 cannot create triangle", calc.canCreateTriangle(20, 3, 4));
  }

  @Test
  public void testCanCreateTriangle_2_31_4_False() {
    Assert.assertFalse("the segments: 2, 31, 4 cannot create triangle", calc.canCreateTriangle(2, 31, 4));
  }

  @Test
  public void testCanCreateTriangle_2_3_40_False() {
    Assert.assertFalse("the segments: 2, 3, 40 cannot create triangle", calc.canCreateTriangle(2, 3, 40));
  }

  @Test
  public void testCanCreateEquilateralTriangle_3_3_3_True() {
    Assert.assertTrue("the segments: 3, 3, 3 can create equilateral triangle", calc.canCreateEquilateralTriangle(3, 3, 3));
  }

  @Test
  public void testCanCreateEquilateralTriangle_5_6_5_False() {
    Assert.assertFalse("the segments: 5, 6, 5 cannot create equilateral triangle", calc.canCreateEquilateralTriangle(5, 6, 5));
  }

  @Test
  public void testCanCreateIsoscelesTriangle_3_3_3_True() {
    Assert.assertTrue("the segments: 3, 3, 3 can create isosceles triangle", calc.canCreateIsoscelesTriangle(3, 3, 3));
  }

  @Test
  public void testCanCreateIsoscelesTriangle_5_6_5_True() {
    Assert.assertTrue("the segments: 5, 6, 5 can create isosceles triangle", calc.canCreateIsoscelesTriangle(5, 6, 5));
  }

  @Test
  public void testCanCreateIsoscelesTriangle_9_6_6_True() {
    Assert.assertTrue("the segments: 9, 6, 6 can create isosceles triangle", calc.canCreateIsoscelesTriangle(9, 6, 6));
  }

  @Test
  public void testCanCreateIsoscelesTriangle_9_9_10_True() {
    Assert.assertTrue("the segments: 9, 9, 10 can create isosceles triangle", calc.canCreateIsoscelesTriangle(9, 9, 10));
  }

  @Test
  public void testCanCreateIsolescesTriangle_9_9_100_False() {
    Assert.assertFalse("the segments: 9, 9, 100 cannot create isosceles triangle", calc.canCreateIsoscelesTriangle(9, 9, 100));
  }

  @Test
  public void testCanCreateIsolescesTriangle_98_99_100_False() {
    Assert.assertFalse("the segments: 98, 99, 100 cannot create isosceles triangle", calc.canCreateIsoscelesTriangle(98, 99, 100));
  }

}
