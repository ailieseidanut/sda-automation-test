# Unit Tests - Best Practices 

The unit test code should be developed with the same care as the basic production code - it makes it easier to maintain the project in the future. 

One of the best practices is the AAA rule. Indicate the best explanation for this abbreviation.

a) American Automobile Association

b) Accumulated Auto Assertions

c) Artificially Arranged Assertions

d) Arrange, Act, Assert