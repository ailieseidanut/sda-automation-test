# Code coverage by tests 

Select 2 most appropriate sentences that accurately describe the code coverage metric. 

a) For condition coverage (`if` statements) it is necessary to perform tests for both the fulfilled and unfulfilled conditions (true, false) 

b) Condition coverage is achieved by executing a line with a given condition, no matter if for true or false 

c) The coverage measure does not answer the question whether the code has been well tested. Can answer the question which parts of the code have not been tested. 

d) The code coverage value agreed for a given organization should be adopted (eg 75%) try to meet this condition during development 

e) Using the TDD technique does not affect the level of code coverage with tests.
