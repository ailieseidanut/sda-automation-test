
# The principle of single responsibility 

The principle of single responsibility applies to: 

a) A single assertion in a unit test 

b) Liability for a test by only one team member 

c) A given unit test is responsible for checking only one feature of the product 

d) An external resource should only do one thing (e.g. only \nwrite data to a file)